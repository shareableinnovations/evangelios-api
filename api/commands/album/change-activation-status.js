var albumDao = require( '../../dao/album.dao' );

exports.execute = function ( id, callback ) {
    albumDao.changeActivationStatus( id, function ( result, err ) {
        callback( result, err );
    });
}