var userDao = require( '../../dao/user.dao' );

exports.execute = function ( userData, callback ) {
    userDao.updateUserFavorites( userData , function ( result, err ) {
        callback( result, err );
    });
}