var mongoose = require('mongoose'),
    dao = require( './dao' ),
    fs = require('fs'),
    util = require('../shared/util'),
    Category = require( '../models/category' ).Category,
    Song = require( '../models/song' ).Song,
    Error = require( '../models/error' );

exports.addCategory = function ( categoryData, callback ) {
    console.log( categoryData );
    dao.connect();
    var newCategory = new Category({
        name: categoryData.name,
        imageUrl: categoryData.image
    });
    newCategory.save( function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var category = {
                id: response._id,
                name: response.name,
                imageUrl: categoryData.image
            }
            callback( category, null );
        }
        dao.disconnect();
    } );
}

exports.getCategoryById = function ( id, callback ) {
    dao.connect();
    Category.findById( id, function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var category = {
                id: response._id,
                name: response.name,
                image: response.imageUrl
            }
            callback( category, null );
        }
        dao.disconnect();
    } );
}

exports.updateCategory = function ( categoryId, categoryData, callback ) {
    console.log( categoryData );
    dao.connect();
    var category = {
        name: categoryData.name,
        imageUrl: categoryData.image
    };
    Category.update( { _id: categoryId }, category, function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            callback( response, null );
        }
        dao.disconnect();
    });
}

exports.getCategories = function ( callback ) {
    dao.connect();
    Category.find( function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var categories = response
                .map((category) => {
                    try {
                        return {
                            id: category._id,
                            name: category.name,
                            image: category.imageUrl
                        };
                    } catch (e) {}
                })
                .filter((category) => {
                    if (category) {
                        return true;
                    } else {
                        return false;
                    }
                });
            Song.aggregate([{$group: {_id: null, totalReproductions: {$sum: '$reproductions'}}}], function (error, resp) {
                console.log(resp);
                if ( err ) {
                    console.log( err );
                    callback( null, new Error( 500, err ) );
                } else {
                    const finalResponse = {
                        categories: categories,
                        totalReproductions: resp[0] && resp[0].totalReproductions || ''
                    }
                    callback( finalResponse, null );
                    dao.disconnect();
                }
            });
        }
    } );
}

exports.deleteCategoryById = function ( categoryId, callback ) {
    dao.connect();
    Category.findById( categoryId, function ( error, response ) {
        if ( error ) {
            console.log( error );
            callback( null, new Error( 500, error ) );
            dao.disconnect();
        } else {
            console.log( response );
            Category.remove( { _id: categoryId }, function ( err, res ) {
                if ( err ) {
                    console.log( err );
                    callback( null, new Error( 500, err ) );
                } else {
                    console.log( res );
                    callback( res, null );
                }
                dao.disconnect();
            });    
        }
    });
}