var mongoose = require('mongoose'),
    bcrypt = require('bcryptjs'),
    dao = require('./dao'),
    User = require('../models/user').User,
    Error = require('../models/error');

exports.getUserByUsernameAndPassword = function (userData, callback) {
    console.log(userData);
    dao.connect();
    User.find()
        .where('username').equals(userData.username)
        .exec(function (err, users) {
            if (err) {
                console.log(err);
                callback(null, new Error(500, err.errmsg));
            } else if (users.length === 0) {
                console.log('Credenciales invalidas');
                callback(null, new Error(400, 'Credenciales invalidas'));
            } else {
                if (bcrypt.compareSync(userData.password, users[0].password)) {
                    console.log(users);
                    callback(users[0], null);
                } else {
                    console.log('Credenciales invalidas');
                    callback(null, new Error(400, 'Credenciales invalidas'));
                }
            }

            dao.disconnect();
        });
}

exports.addUser = function (userData, callback) {
    console.log(userData);
    dao.connect();
    var newUser = new User(userData);
    newUser.save(function (err, response) {
        if (err) {
            console.log(err);
            var message = err.errmsg;
            if (err.code === 11000) {
                if (err.errmsg.includes('email')) {
                    message = 'Este correo ya se encuentra en uso';
                } else {
                    message = 'Este correo ya se encuentra en uso';
                }
            }
            callback(null, new Error(500, message));
        } else {
            console.log(response);
            callback(response, null);
        }

        dao.disconnect();
    });
}

exports.updateUserFavorites = function (userData, callback) {
    console.log(userData);
    dao.connect();
    User.findById(userData._id, function(err, user){
        console.log('lo encontre')
        //make changes accordingly
        user.favorites = userData.favorites;
        user.save(function(err){
            console.log('saved')
            callback(user);
        })
    });
}

exports.updateUser = function (userData, callback) {
    console.log(userData);
    dao.connect();
    const user = {
        firstName: userData.firstName,
        lastName: userData.lastName,
        email: userData.email,
        username: userData.email,
        type: userData.type,
        role: userData.role,
        favorites: userData.favorites,
        image: userData.image
    };
    User.update( { _id: userData._id }, user, function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            callback( response, null );
        }
        dao.disconnect();
    });
}

exports.updateUserPassword = function ( email, password, callback ) {
    dao.connect();
    User.findOne({email: email}, function (err, response) {
        if (err) {
            console.log( err );
            callback( null, new Error( 500, err ) );
            dao.disconnect();
        } else {
            response.password = password;
            response.save(function(err){
                if (err) {
                    console.log( err );
                    callback( null, new Error( 500, err ) );
                } else {
                    callback(response, null);
                }
                dao.disconnect();
            });
        }
    });
}

exports.getUsers = function ( callback ) {
    dao.connect();
    User.find( function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var users = response
                .map((user) => {
                    try {
                        return {
                            id: user._id,
                            firstName: user.firstName,
                            lastName: user.lastName,
                            email: user.email,
                            username: user.username,
                            type: user.type,
                            role: user.role
                        };
                    } catch (e) {}
                })
                .filter((user) => {
                    if (user) {
                        return true;
                    } else {
                        return false;
                    }
                });
            callback( users, null );
        }
        dao.disconnect();
    } );
}

exports.deleteUserById = function ( userId, callback ) {
    dao.connect();
    User.findById( userId, function ( error, response ) {
        if ( error ) {
            console.log( error );
            callback( null, new Error( 500, error ) );
            dao.disconnect();
        } else {
            console.log( response );
            User.remove( { _id: userId }, function ( err, res ) {
                if ( err ) {
                    console.log( err );
                    callback( null, new Error( 500, err ) );
                } else {
                    console.log( res );
                    callback( res, null );
                }
                dao.disconnect();
            });    
        }
    });
}
