var userDao = require( '../../dao/user.dao' );

exports.execute = function ( userId, callback ) {
    userDao.deleteUserById( userId , function ( result, err ) {
        callback( result, err );
    });
}