var userDao = require( '../../dao/user.dao' ),
    bcrypt = require('bcryptjs');

exports.execute = function ( email, callback ) {
    var nodemailer = require('nodemailer');
    var password = generatePassword();
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'spotiyisusapp@gmail.com',
            pass: 'adminspotiyisus'
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    

    var mailOptions = {
        from: 'spotiyisusapp@gmail.com',
        to: email,
        subject: 'Spotiyisus - Recuperación de contraseña',
        text: 'Se ha generado una nueva contraseña. Contraseña: ' + password
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            callback(null, new Error( 500, error ));
        } else {
            password = bcrypt.hashSync(password, 8);
            userDao.updateUserPassword(email, password, function (result, err) {
                callback(result, err);
            });
        }
    });
}

function generatePassword () {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    
    for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    
    return text;
}