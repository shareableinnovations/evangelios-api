var mongoose = require( 'mongoose' );

exports.connect = function () {
    mongoose.connect('mongodb://localhost/jesusnazaret');
};

exports.disconnect = function () {
    mongoose.connection.close();
}

exports.audioFilesPath = 'static/audio/';
exports.imageFilesPath = 'static/image/';
exports.lyricFilesPath = 'static/lyric/';
