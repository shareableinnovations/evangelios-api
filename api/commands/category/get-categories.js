var categoryDao = require( '../../dao/category.dao' );

exports.execute = function ( callback ) {
    categoryDao.getCategories( function ( result, err ) {
        callback( result, err );
    });
}