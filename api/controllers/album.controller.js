var addAlbumCommand = require( '../commands/album/add-album' );
var getAlbumByIdCommand = require( '../commands/album/get-album-by-id' );
var updateAlbumCommand = require( '../commands/album/update-album' );
var getAlbumsCommand = require( '../commands/album/get-albums' );
var changeActivationStatusCommand = require( '../commands/album/change-activation-status' );
var deleteAlbumByIdCommand = require( '../commands/album/delete-album-by-id' );

exports.addAlbum = function ( albumData, callback ) {
    addAlbumCommand.execute( albumData, function ( response, err ) {
        callback( response, err );
    });
}

exports.getAlbumById = function ( albumId, callback ) {
    getAlbumByIdCommand.execute( albumId, function ( response, err ) {
        callback( response, err );
    });
}

exports.updateAlbum = function ( albumId, albumData, callback ) {
    updateAlbumCommand.execute( albumId, albumData, function ( response, err ) {
        callback( response, err );
    });
}

exports.getAlbums = function ( callback ) {
    getAlbumsCommand.execute( function ( response, err ) {
        callback( response, err );
    });
}

exports.changeActivationStatus = function ( albumId, callback ) {
    changeActivationStatusCommand.execute( albumId, function ( response, err ) {
        callback( response, err );
    });
}

exports.deleteAlbumById = function ( albumId, callback ) {
    deleteAlbumByIdCommand.execute( albumId, function ( response, err ) {
        callback( response, err );
    });
}