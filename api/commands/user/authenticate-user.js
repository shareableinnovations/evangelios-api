var userDao = require( '../../dao/user.dao' );
var generateTokenCommand = require( '../auth/generate-token' );

exports.execute = function ( user, callback ) {
    userDao.getUserByUsernameAndPassword( user , function ( result, err ) {
        if ( err ) {
            callback( result, err );
        } else {
            var token = generateTokenCommand.execute( result );
            callback( { user: result, token: token }, err );
        }
    });
}