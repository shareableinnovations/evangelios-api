var addReproductionCommand = require( '../commands/reproduction/add-reproduction' );
var getReproductionsCommand = require( '../commands/reproduction/get-reproductions' );

exports.addReproduction = function ( reproductionData, callback ) {
    addReproductionCommand.execute( reproductionData, function ( response, err ) {
        callback( response, err );
    });
}

exports.getReproductions = function ( callback ) {
    getReproductionsCommand.execute( function ( response, err ) {
        callback( response, err );
    });
}