var songDao = require( '../../dao/song.dao' );

exports.execute = function ( song, callback ) {
    songDao.addSong( song , function ( result, err ) {
        callback( result, err );
    });
}