var mongoose = require('mongoose'),
    dao = require( './dao' ),
    Album = require( '../models/album' ).Album,
    Song = require( '../models/song' ).Song,
    Error = require( '../models/error' );

exports.addAlbum = function ( albumData, callback ) {
    console.log( albumData );
    dao.connect();
    var newAlbum = new Album({
        name: albumData.name,
        imageUrl: albumData.image,
        isActive: true,
        lastModify: new Date()
    });
    newAlbum.save( function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var album = {
                id: response._id,
                name: response.name,
                imageUrl: albumData.image,
                isActive: response.isActive,
                lastModify: response.lastModify
            }
            callback( album, null );
        }
        dao.disconnect();
    } );
}

exports.getAlbumById = function ( id, callback ) {
    dao.connect();
    Album.findById( id, function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var album = {
                id: response._id,
                name: response.name,
                image: response.imageUrl,
                isActive: response.isActive,
                lastModify: response.lastModify
            }
            callback( album, null );
        }
        dao.disconnect();
    } );
}

exports.updateAlbum = function ( albumId, albumData, callback ) {
    console.log( albumData );
    dao.connect();
    const album = {
        name: albumData.name,
        imageUrl: albumData.image,
        isActive: albumData.isActive,
        lastModify: new Date()
    };
    Album.update( { _id: albumId }, album, function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            callback( response, null );
        }
        dao.disconnect();
    });
}

exports.getAlbums = function ( callback ) {
    dao.connect();
    Album.find( function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var albums = response
                .map((album) => {
                    try {

                        return {
                            id: album._id,
                            name: album.name,
                            image: album.imageUrl,
                            isActive: album.isActive,
                            lastModify: album.lastModify,
                            songCount: 0
                        };
                    } catch (e) {}
                })
                .filter((album) => {
                    if (album) {
                        return true;
                    } else {
                        return false;
                    }
                });
        }

            Song.aggregate([{$group: {_id: null, totalReproductions: {$sum: '$reproductions'}}}], function (error, resp) {
                console.log(resp);
                if ( err ) {
                    console.log( err );
                    callback( null, new Error( 500, err ) );
                } else {

                    const finalResponse = {
                        albums: albums,
                        totalReproductions: resp[0] && resp[0].totalReproductions || ''
                    }
                    callback( finalResponse, null );
                    dao.disconnect();
                }
            });
    } );
}

exports.changeActivationStatus = function ( albumId, callback ) {
    dao.connect();

    Album.findById( albumId, function ( error, response ) {
        if ( error ) {
            console.log( error );
            callback( null, new Error( 500, error ) );
            dao.disconnect();
        } else {
            console.log( response );
            Album.update( { _id: albumId }, { isActive: !response.isActive }, function ( err, res ) {
                if ( err ) {
                    console.log( err );
                    callback( null, new Error( 500, err ) );
                } else {
                    console.log( res );
                    callback( response, null );
                }
                dao.disconnect();
            });     
        }
    });
}

exports.deleteAlbumById = function ( albumId, callback ) {
    dao.connect();
    Album.findById( albumId, function ( error, response ) {
        if ( error ) {
            console.log( error );
            callback( null, new Error( 500, error ) );
            dao.disconnect();
        } else {
            console.log( response );
            Album.remove( { _id: albumId }, function ( err, res ) {
                if ( err ) {
                    console.log( err );
                    callback( null, new Error( 500, err ) );
                } else {
                    console.log( res );
                    callback( res, null );
                }
                dao.disconnect();
            });    
        }
    });
}