var express = require( 'express' ),
    albumsRouter = express.Router(),
    Response = require( '../models/response' ),
    auth = require( '../middlewares/auth.middleware' ),
    Error = require( '../models/error' );

var albumController = require( '../controllers/album.controller' );
albumsRouter
    // Add an album
    .post( '/', function ( req, res, next ) { 
        console.log( 'POST Request to /albums' );

        req.checkBody( 'name' ).notEmpty();
        req.checkBody( 'image' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            albumController.addAlbum(req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })

    // Get album by id
    .get( '/:id', function ( req, res, next ) {
        console.log( 'GET Request to /albums/:id' ); 

        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Data faltantes' ) )
        } else {
            console.log(req.params);
            albumController.getAlbumById(req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })
    
    // Update an album
    .put( '/:id', function ( req, res, next ) { 
        console.log( 'PUT Request to /albums/:id' );
        
        req.checkParams('id').notEmpty();
        req.checkBody( 'name' ).notEmpty();
        req.checkBody( 'image' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            albumController.updateAlbum(req.params.id, req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })
    
    // Get albums
    .get( '/', function ( req, res, next ) {
        console.log( 'GET Request to /albums' ); 

        albumController.getAlbums(function ( resp, err ) {
            if ( err ) {
                next( err );
            } else {
                var response = new Response( resp, null );
                res.json( response );
            }
        });
    })
    
    // Update an album activation status
    .put( '/:id/activation', function ( req, res, next ) { 
        console.log( 'PUT Request to /albums/:id/activation' );
        
        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            albumController.changeActivationStatus(req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })
    
    //Delete album by id
    .delete( '/:id', function ( req, res, next) {
        console.log( 'DELETE Request to /albums/:id' ); 
        
        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();
        
        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) );
        } else {
            albumController.deleteAlbumById( req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    });

module.exports = albumsRouter;