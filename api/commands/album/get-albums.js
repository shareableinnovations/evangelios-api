var albumDao = require( '../../dao/album.dao' );

exports.execute = function ( callback ) {
    albumDao.getAlbums( function ( result, err ) {
        callback( result, err );
    });
}