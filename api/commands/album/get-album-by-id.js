var albumDao = require( '../../dao/album.dao' );

exports.execute = function ( albumId, callback ) {
    albumDao.getAlbumById( albumId , function ( result, err ) {
        callback( result, err );
    });
}