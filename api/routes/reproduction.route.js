var express = require('express'),
    reproductionRouter = express.Router(),
    Response = require('../models/response'),
    auth = require('../middlewares/auth.middleware'),
    Error = require('../models/error');

var reproductionController = require('../controllers/reproduction.controller');
    reproductionRouter
        // Get reproductions
        .get('/', function (req, res, next) {
            console.log('GET Request to /reproductions');

            reproductionController.getReproductions(function (resp, err) {
                if (err) {
                    next(err);
                } else {
                    var response = new Response(resp, null);
                    res.json(response);
                }
            });
        })

        .post('/', function (req, res, next) {
            console.log('POST Request to /users');

            req.checkBody('user').notEmpty();
            req.checkBody('song').notEmpty();
            req.checkBody('albums').notEmpty();

            var errors = req.validationErrors();

            if (errors) {
                next(new Error(400, 'Parametros faltantes'))
            } else {
                reproductionController.addReproduction(req.body, function (resp, err) {
                    if (err) {
                        next(err);
                    } else {
                        var response = new Response(resp, null);
                        res.json(response);
                    }
                });
            }
        });

module.exports = reproductionRouter;