var albumDao = require( '../../dao/album.dao' );

exports.execute = function ( id, album, callback ) {
    albumDao.updateAlbum( id, album , function ( result, err ) {
        callback( result, err );
    });
}