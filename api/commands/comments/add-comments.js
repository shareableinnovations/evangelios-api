var bcrypt = require('bcryptjs');
var commentDao = require( '../../dao/comment.dao' );

exports.execute = function ( comment, callback ) {
    commentDao.addComment( comment , function ( result, err ) {
        callback( result, err );
    });
}