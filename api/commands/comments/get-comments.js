var commentsDao = require( '../../dao/comment.dao' );

exports.execute = function ( callback ) {
    commentsDao.getComments( function ( result, err ) {
        callback( result, err );
    });
}