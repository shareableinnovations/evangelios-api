var express = require( 'express' ),
    bodyParser = require( 'body-parser' ),
    expressValidator = require( 'express-validator' ),
    app = express(),
    port = process.env.PORT || 9000;

app.use( bodyParser.urlencoded( { extended: true, limit: '10mb' } ) );
app.use( bodyParser.json( { limit: '10mb' } ) );
app.use( expressValidator() );

var accessControl = require( './api/middlewares/access-control.middleware' );
app.use( accessControl );

var router = require( './api/routes/routes' );
app.use( '/rest-api', router );

var errorHandler = require( './api/middlewares/error-handler.middleware' );
app.use( errorHandler );

app.listen( port );
console.log( 'RESTful API Server started on:', port );