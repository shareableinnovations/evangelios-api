var jwt = require( 'jsonwebtoken' ),
    config = require( '../../config' ),
    Error = require( '../models/error' );

module.exports = function(req, res, next) {
    var token = req.headers['x-access-token'];
    console.log('auth.middleware');
    jwt.verify(token, config.secret, function (err, decoded) {
        if ( err ) {
            next( new Error( 400, 'Invalid token' ) );
        } else {
            next();
        }
    });
};