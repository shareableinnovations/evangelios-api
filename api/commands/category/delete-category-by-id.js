var categoryDao = require( '../../dao/category.dao' );

exports.execute = function ( categoryId, callback ) {
    categoryDao.deleteCategoryById( categoryId , function ( result, err ) {
        callback( result, err );
    });
}