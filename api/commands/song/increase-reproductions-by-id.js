var songDao = require( '../../dao/song.dao' ),
    reproductionDao = require('../../dao/reproduction.dao');


exports.execute = function ( songId, user, callback ) {
    songDao.increaseSongReproductionsById( songId , function ( result, err ) {
        if ( err ) {
            callback( result, err );
        } else {
            var reproduction = {
                user: user,
                song: result.name,
                albums: result.albums.map((album) => { return album.name; })
            };
            
            reproductionDao.addReproduction(reproduction, function ( response, error ) {
                callback( result, error );
            });
        }
    });
}