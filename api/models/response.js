var Response = function ( data, error ) {
    this.data = data;
    this.error = error;
}

module.exports = Response;