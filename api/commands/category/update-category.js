var categoryDao = require( '../../dao/category.dao' );

exports.execute = function ( id, category, callback ) {
    categoryDao.updateCategory( id, category , function ( result, err ) {
        callback( result, err );
    });
}