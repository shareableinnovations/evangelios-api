var addCategoryCommand = require( '../commands/category/add-category' );
var getCategoryByIdCommand = require( '../commands/category/get-category-by-id' );
var updateCategoryCommand = require( '../commands/category/update-category' );
var getCategoriesCommand = require( '../commands/category/get-categories' );
var deleteCategoryByIdCommand = require( '../commands/category/delete-category-by-id' );

exports.addCategory = function ( categoryData, callback ) {
    addCategoryCommand.execute( categoryData, function ( response, err ) {
        callback( response, err );
    });
}

exports.getCategoryById = function ( categoryId, callback ) {
    getCategoryByIdCommand.execute( categoryId, function ( response, err ) {
        callback( response, err );
    });
}

exports.updateCategory = function ( categoryId, categoryData, callback ) {
    updateCategoryCommand.execute( categoryId, categoryData, function ( response, err ) {
        callback( response, err );
    });
}

exports.getCategories = function ( callback ) {
    getCategoriesCommand.execute( function ( response, err ) {
        callback( response, err );
    });
}

exports.deleteCategoryById = function ( categoryId, callback ) {
    deleteCategoryByIdCommand.execute( categoryId, function ( response, err ) {
        callback( response, err );
    });
}