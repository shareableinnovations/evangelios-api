var express = require( 'express' ),
    songsRouter = express.Router(),
    Response = require( '../models/response' ),
    auth = require( '../middlewares/auth.middleware' ),
    Error = require( '../models/error' );

var songController = require( '../controllers/song.controller' );
songsRouter
    // Add an song
    .post( '/', function ( req, res, next ) { 
        console.log( 'POST Request to /songs' );

        req.checkBody( 'name' ).notEmpty();
        req.checkBody( 'image' ).notEmpty();
        req.checkBody( 'audio' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            songController.addSong(req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })

    // Get song by id
    .get( '/:id', function ( req, res, next ) {
        console.log( 'GET Request to /songs/:id' ); 

        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Data faltante' ) )
        } else {
            console.log(req.params);
            songController.getSongById(req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })
    
    // Update an song
    .put( '/:id', function ( req, res, next ) { 
        console.log( 'PUT Request to /songs/:id' );
        
        req.checkParams('id').notEmpty();
        req.checkBody( 'name' ).notEmpty();
        req.checkBody( 'image' ).notEmpty();
        req.checkBody( 'audio' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            songController.updateSong(req.params.id, req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })
    
    // Get songs
    .get( '/', function ( req, res, next ) {
        console.log( 'GET Request to /songs' ); 

        songController.getSongs(function ( resp, err ) {
            if ( err ) {
                next( err );
            } else {
                var response = new Response( resp, null );
                res.json( response );
            }
        });
    })

    // get song album
    .get( '/:id/album', function ( req, res, next ) {
        console.log( 'GET Request to /songs' );

        songController.getSongAlbum(req.params.id,function ( resp, err ) {
            if ( err ) {
                next( err );
            } else {
                var response = new Response( resp, null );
                res.json( response );
            }
        });
    })

    // get song album count
    .get( '/:id/album/count', function ( req, res, next ) {
        console.log( 'GET Request to /songs' );

        songController.getSongAlbumCount(req.params.id,function ( resp, err ) {
            if ( err ) {
                next( err );
            } else {
                var response = new Response( resp, null );
                res.json( response );
            }
        });
    })
    
    // Update an song activation status
    .put( '/:id/activation', function ( req, res, next ) { 
        console.log( 'PUT Request to /songs/:id/activation' );
        
        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            songController.changeActivationStatus(req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })
    
    //Delete song by id
    .delete( '/:id', function ( req, res, next) {
        console.log( 'DELETE Request to /songs/:id' ); 
        
        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();
        
        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) );
        } else {
            songController.deleteSongById( req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })
    
    // Update an song reproductions quantity
    .put( '/:id/reproductions', function ( req, res, next ) { 
        console.log( 'PUT Request to /songs/:id/reproductions' );
        
        req.checkParams('id').notEmpty();
        req.checkQuery('user').notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            songController.increaseSongReproductionsById(req.params.id, req.query.user, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    });

module.exports = songsRouter;