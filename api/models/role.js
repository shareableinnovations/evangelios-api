var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoleSchema = new Schema({
    name: String
});

exports.Role = mongoose.model('Role', RoleSchema);