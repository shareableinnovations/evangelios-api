var bcrypt = require('bcryptjs');
var userDao = require( '../../dao/user.dao' );

exports.execute = function ( user, callback ) {
    user.password = bcrypt.hashSync(user.password, 8);
    user.username = user.email;
    userDao.addUser( user , function ( result, err ) {
        callback( result, err );
    });
}