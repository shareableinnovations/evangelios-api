var jwt = require('jsonwebtoken'),
    config = require('../../../config');

exports.execute = function ( user ) {
    return jwt.sign({ id: user._id }, config.secret, {
        expiresIn: 86400 // expires in 24 hours
    });
}