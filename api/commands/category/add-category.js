var categoryDao = require( '../../dao/category.dao' );

exports.execute = function ( category, callback ) {
    categoryDao.addCategory( category , function ( result, err ) {
        callback( result, err );
    });
}