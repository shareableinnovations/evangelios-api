var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
    name: String,
    imageUrl: String
});

exports.Category = mongoose.model('Category', CategorySchema);