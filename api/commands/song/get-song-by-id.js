var songDao = require( '../../dao/song.dao' );

exports.execute = function ( songId, callback ) {
    songDao.getSongById( songId , function ( result, err ) {
        callback( result, err );
    });
}