var categoryDao = require( '../../dao/category.dao' );

exports.execute = function ( categoryId, callback ) {
    categoryDao.getCategoryById( categoryId , function ( result, err ) {
        callback( result, err );
    });
}