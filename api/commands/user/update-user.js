var userDao = require( '../../dao/user.dao' );

exports.execute = function ( userData, callback ) {
    userDao.updateUser( userData , function ( result, err ) {
        callback( result, err );
    });
}