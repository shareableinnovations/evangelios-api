var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    firstName: String,
    lastName: String,
    email: { type: String, unique: true },
    username: { type: String, unique: true },
    type: String,
    password: String,
    role: String,
    image: String,
    favorites: [{ type: Schema.Types.ObjectId, ref: 'Album' }],
});

exports.User = mongoose.model('User', UserSchema);