var getCommentsCommand = require( '../commands/comments/get-comments' );
var addCommentCommand = require('../commands/comments/add-comments')
var Comment = require( '../models/comment' );

exports.getComments = function ( callback ) {
    getCommentsCommand.execute( function ( response, err ) {
        callback( response, err );
    });
}

exports.addComment = function ( commentData, callback ) {
    addCommentCommand.execute( commentData, function (response, err ) {
        callback( response, err );
    });
}