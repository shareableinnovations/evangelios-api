var songDao = require( '../../dao/song.dao' );

exports.execute = function (albumId, callback ) {
    songDao.getSongAlbumCount(albumId, function ( result, err ) {
        console.log(result)
        callback( result, err );
    });
}