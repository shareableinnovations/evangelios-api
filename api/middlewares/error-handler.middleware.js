var Response = require( '../models/response' );

module.exports = function (err, req, res, next) {
    var status = err.status || 500;
    var message = err.message || err;
    res.status( status ).json( new Response( null, message ) );
}