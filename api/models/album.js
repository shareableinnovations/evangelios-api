var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AlbumSchema = new Schema({
    name: String,
    isActive: Boolean,
    lastModify: Date,
    imageUrl: String,
    songsCount: String,
});

exports.Album = mongoose.model('Album', AlbumSchema);