var userDao = require( '../../dao/user.dao' );

exports.execute = function ( callback ) {
    userDao.getUsers( function ( result, err ) {
        callback( result, err );
    });
}