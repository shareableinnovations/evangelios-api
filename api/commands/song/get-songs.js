var songDao = require( '../../dao/song.dao' );

exports.execute = function ( callback ) {
    songDao.getSongs( function ( result, err ) {
        console.log(result)
        callback( result, err );
    });
}