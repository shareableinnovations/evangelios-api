var albumDao = require( '../../dao/album.dao' );

exports.execute = function ( album, callback ) {
    albumDao.addAlbum( album , function ( result, err ) {
        callback( result, err );
    });
}