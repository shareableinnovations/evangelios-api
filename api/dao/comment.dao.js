var mongoose = require('mongoose'),
    bcrypt = require('bcryptjs'),
    dao = require('./dao'),
    Comments = require('../models/comment').Comment,
    Error = require('../models/error');

exports.addComment = function (commentData, callback) {
    console.log(commentData);
    dao.connect();
    var newComment = new Comments(commentData);
    newComment.save(function (err, response) {
        if (err) {
            console.log(err);
            var message = err.errmsg;
            callback(null, new Error(500, message));
        } else {
            console.log(response);
            callback(response, null);
        }

        dao.disconnect();
    });
}

exports.getComments = function ( callback ) {
    dao.connect();
    Comments.find( function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var comments = response
                .map((comment) => {
                    try {
                        return {
                            user: comment.user,
                            comment: comment.comment,
                            email: comment.email,
                            date: comment.date
                        };
                    } catch (e) {}
                })
                .filter((comment) => {
                    if (comment) {
                        return true;
                    } else {
                        return false;
                    }
                });
            callback( comments, null );
        }
        dao.disconnect();
    } );
}