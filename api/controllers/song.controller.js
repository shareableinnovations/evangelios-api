var addSongCommand = require( '../commands/song/add-song' );
var getSongByIdCommand = require( '../commands/song/get-song-by-id' );
var updateSongCommand = require( '../commands/song/update-song' );
var getSongsCommand = require( '../commands/song/get-songs' );
var getSongAlbumCommand = require( '../commands/song/get-song-album' );
var getSongAlbumCommandCount = require( '../commands/song/get-song-album-count' );
var changeActivationStatusCommand = require( '../commands/song/change-activation-status' );
var deleteSongByIdCommand = require( '../commands/song/delete-song-by-id' );
var increaseSongReproductionsByIdCommand = require( '../commands/song/increase-reproductions-by-id' );

exports.addSong = function ( songData, callback ) {
    addSongCommand.execute( songData, function ( response, err ) {
        callback( response, err );
    });
}

exports.getSongById = function ( songId, callback ) {
    getSongByIdCommand.execute( songId, function ( response, err ) {
        callback( response, err );
    });
}

exports.updateSong = function ( songId, songData, callback ) {
    updateSongCommand.execute( songId, songData, function ( response, err ) {
        callback( response, err );
    });
}

exports.getSongs = function ( callback ) {
    getSongsCommand.execute( function ( response, err ) {
        callback( response, err );
    });
}

exports.getSongAlbum = function (albumId, callback ) {
    getSongAlbumCommand.execute(albumId, function ( response, err ) {
        callback( response, err );
    });
}

exports.getSongAlbumCount = function (albumId, callback ) {
    getSongAlbumCommandCount.execute(albumId, function ( response, err ) {
        callback( response, err );
    });
}

exports.changeActivationStatus = function ( songId, callback ) {
    changeActivationStatusCommand.execute( songId, function ( response, err ) {
        callback( response, err );
    });
}

exports.deleteSongById = function ( songId, callback ) {
    deleteSongByIdCommand.execute( songId, function ( response, err ) {
        callback( response, err );
    });
}

exports.increaseSongReproductionsById = function ( songId, user, callback ) {
    increaseSongReproductionsByIdCommand.execute( songId, user, function ( response, err ) {
        callback( response, err );
    });
}