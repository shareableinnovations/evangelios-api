var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReproductionSchema = new Schema({
    user: String,
    song: String,
    albums: [ String ]
});

exports.Reproduction = mongoose.model('Reproduction', ReproductionSchema);