var mongoose = require('mongoose'),
    dao = require( './dao' ),
    Reproduction = require( '../models/reproduction' ).Reproduction,
    Error = require( '../models/error' );

exports.addReproduction = function ( reproductionData, callback ) {
    dao.connect();
    var newReproduction = new Reproduction({
        user: reproductionData.user,
        song: reproductionData.song,
        albums: reproductionData.albums
    });
    newReproduction.save( function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            callback( response, null );
        }
        dao.disconnect();
    } );
}

exports.getReproductions = function ( callback ) {
    dao.connect();
    Reproduction.find({}).exec( function ( err, response ) {
        if ( err ) {
            console.log( err );
            callback( null, new Error( 500, err ) );
        } else {
            console.log( response );
            var reproductions = response
                .map((reproduction, index) => {
                    try {
                        return {
                            id: index,
                            user: reproduction.user,
                            song: reproduction.song,
                            albums: reproduction.albums
                        };
                    } catch (e) {}
                }).reverse();
            
            callback( reproductions, null );
            dao.disconnect();
        }
    } );
}