var reproductionDao = require( '../../dao/reproduction.dao' );

exports.execute = function ( callback ) {
    reproductionDao.getReproductions( function ( result, err ) {
        callback( result, err );
    });
}