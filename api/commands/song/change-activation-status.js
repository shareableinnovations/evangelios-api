var songDao = require( '../../dao/song.dao' );

exports.execute = function ( id, callback ) {
    songDao.changeActivationStatus( id, function ( result, err ) {
        callback( result, err );
    });
}