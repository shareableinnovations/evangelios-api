var express = require( 'express' ),
    router = express.Router(),
    auth = require( '../middlewares/auth.middleware' );

var usersRouter = require( './user.route' );
router.use( '/users', usersRouter);

var albumsRouter = require( './album.route' );
router.use( '/albums', albumsRouter);

var categoriesRouter = require( './category.route' );
router.use( '/categories', categoriesRouter);

var songsRouter = require( './song.route' );
router.use( '/songs', songsRouter);

var commentRouter = require('./comment.route');
router.use('/comments', commentRouter);

var reproductionRouter = require('./reproduction.route');
router.use('/reproductions', reproductionRouter);

module.exports = router;