var reproductionDao = require( '../../dao/reproduction.dao' );

exports.execute = function ( reproduction, callback ) {
    reproductionDao.addReproduction( reproduction , function ( result, err ) {
        callback( result, err );
    });
}