var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CompanySchema = new Schema({
    name: String
});

exports.Company = mongoose.model('Company', CompanySchema);