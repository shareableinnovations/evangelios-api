var authenticateUserCommand = require( '../commands/user/authenticate-user' );
var addUserCommand = require( '../commands/user/add-user' );
var updateUserCommand = require( '../commands/user/update-user' );
var updateUserFavoritesCommand = require( '../commands/user/update-user-favorites' );
var getUsersCommand = require( '../commands/user/get-users' );
var deleteUserByIdCommand = require( '../commands/user/delete-user-by-id' );
var recoverPasswordCommand = require( '../commands/user/recover-password' );
var User = require( '../models/user' );

exports.authenticateUser = function ( username, password, callback ) {
    var user = {
        username: username,
        password: password
    };

    authenticateUserCommand.execute( user, function ( response, err ) {
        callback( response, err );
    });
}

exports.addUser = function ( userData, callback ) {
    addUserCommand.execute( userData, function (response, err ) {
        callback( response, err );
    });
}

exports.updateUserFavorites = function ( userData, callback ) {
    updateUserFavoritesCommand.execute( userData, function (response, err ) {
        callback( response, err );
    });
}

exports.updateUser = function ( userData, callback ) {
    updateUserCommand.execute( userData, function (response, err ) {
        callback( response, err );
    });
}

exports.getUsers = function ( callback ) {
    getUsersCommand.execute( function ( response, err ) {
        callback( response, err );
    });
}

exports.deleteUserById = function ( userId, callback ) {
    deleteUserByIdCommand.execute( userId, function ( response, err ) {
        callback( response, err );
    });
}

exports.recoverPassword = function ( email, callback ) {
    recoverPasswordCommand.execute( email, function ( response, err ) {
        callback( response, err );
    });
}