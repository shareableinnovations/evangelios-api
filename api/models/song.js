var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SongSchema = new Schema({
    name: String,
    isActive: Boolean,
    length: Number,
    imageUrl: String,
    audioUrl: String,
    lyricUrl: String,
    reproductions: Number,
    albums: [{ type: Schema.Types.ObjectId, ref: 'Album' }],
    categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }]
});

exports.Song = mongoose.model('Song', SongSchema);