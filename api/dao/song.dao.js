var mongoose = require('mongoose'),
    fs = require('fs'),
    util = require('../shared/util'),
    dao = require('./dao'),
    Song = require('../models/song').Song,
    Error = require('../models/error');

exports.addSong = function (songData, callback) {
    console.log(songData);
    dao.connect();
    var newSong = new Song({
        name: songData.name,
        length: songData.length,
        imageUrl: songData.image,
        audioUrl: songData.audio,
        lyricUrl: songData.lyric.url,
        albums: songData.albums,
        categories: songData.categories,
        isActive: true
    });
    newSong.save(function (err, response) {
        if (err) {
            console.log(err);
            callback(null, new Error(500, err));
        } else {
            console.log(response);

            if (songData.lyric.data)
            {
                var lyricBuffer = util.dataUriToBuffer(songData.lyric.data);
                fs.writeFileSync(dao.lyricFilesPath + response._id + '.txt', lyricBuffer);
            }

            var song = {
                id: response._id,
                name: response.name,
                image: songData.image,
                audio: songData.audio,
                lyric: songData.lyric,
                isActive: response.isActive,
                length: response.length
            }
            callback(song, null);
        }
        dao.disconnect();
    });
}

exports.getSongById = function (id, callback) {
    dao.connect();
    Song.findById(id).populate('albums').populate('categories')
        .exec(function (err, response) {
            if (err) {
                console.log(err);
                callback(null, new Error(500, err));
            } else {
                console.log(response);

                const lyricData = util.getDataUriFromPath(dao.lyricFilesPath + response._id + '.txt', 'text/plain');

                var song = {
                    id: response._id,
                    name: response.name,
                    image: response.imageUrl,
                    audio: response.audioUrl,
                    lyric: {
                        url: response.lyricUrl,
                        data: lyricData
                    },
                    isActive: response.isActive,
                    length: response.length,
                    reproductions: response.reproductions
                }
                callback(song, null);
            }
            dao.disconnect();
        });
}

exports.updateSong = function (songId, songData, callback) {
    console.log(songData);
    dao.connect();
    const song = {
        name: songData.name,
        imageUrl: songData.image,
        audioUrl: songData.audio,
        lyricUrl: songData.lyric.url,
        albums: songData.albums,
        categories: songData.categories,
        isActive: songData.isActive,
        length: songData.length
    };
    Song.update({ _id: songId }, song, function (err, response) {
        if (err) {
            console.log(err);
            callback(null, new Error(500, err));
        } else {
            console.log(response);

            if (songData.lyric.data)
            {
                var lyricBuffer = util.dataUriToBuffer(songData.lyric.data);
                fs.writeFileSync(dao.lyricFilesPath + songId + '.txt', lyricBuffer);
            }

            callback(response, null);
        }
        dao.disconnect();
    });
}

exports.getSongs = function ( callback ) {
    dao.connect();
    Song.find().populate( 'albums' ).populate('categories')
        .exec( function ( err, response ) {
            if ( err ) {
                console.log( err );
                callback( null, new Error( 500, err ) );
            } else {
                console.log( response );
                var songs = response
                    .map((song) => {
                        const lyricData = util.getDataUriFromPath( dao.lyricFilesPath + song.id + '.txt', 'text/plain' );
                        try {
                            return {
                                id: song._id,
                                name: song.name,
                                image: song.imageUrl,
                                audio: song.audioUrl,
                                lyric: {
                                    url: song.lyricUrl,
                                    data: lyricData
                                },
                                categories: song.categories,
                                albums: song.albums,
                                isActive: song.isActive,
                                length: song.length,
                                reproductions: song.reproductions
                            };
                        } catch (e) {}
                    })
                    .filter((song) => {
                        if (song) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                callback( songs, null );
            }
            dao.disconnect();
        } );
}

exports.getSongAlbum = function (id,  callback ) {
    dao.connect();
    Song.find({ albums: id  }).populate( 'albums' ).populate('categories')
        .exec( function ( err, response ) {
            if ( err ) {
                console.log( err );
                callback( null, new Error( 500, err ) );
            } else {
                console.log( response );
                var songs = response
                    .map((song) => {
                        const lyricData = util.getDataUriFromPath( dao.lyricFilesPath + song._id + '.txt', 'text/plain' );
                        try {
                            return {
                                id: song._id,
                                name: song.name,
                                image: song.imageUrl,
                                audio: song.audioUrl,
                                lyric: {
                                    url: song.lyricUrl,
                                    data: lyricData
                                },
                                categories: song.categories,
                                albums: song.albums,
                                isActive: song.isActive,
                                length: song.length,
                                reproductions: song.reproductions
                            };
                        } catch (e) {}
                    })
                    .filter((song) => {
                        if (song) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                callback( songs, null );
            }
            dao.disconnect();
        } );
}

exports.getSongAlbumCount = function (id,  callback ) {
    dao.connect();
    Song.find({ albums: id  })
        .count( function ( err, response ) {
            if ( err ) {
                console.log( err );
                callback( null, new Error( 500, err ) );
            } else {
                console.log( response );
                var songs = {
                    album_id: id,
                    count: response,
                };
                callback( songs, null );
            }
            dao.disconnect();
        } );
}

exports.changeActivationStatus = function (songId, callback) {
    dao.connect();

    Song.findById(songId, function (error, response) {
        if (error) {
            console.log(error);
            callback(null, new Error(500, error));
            dao.disconnect();
        } else {
            console.log(response);
            Song.update({ _id: songId }, { isActive: !response.isActive }, function (err, res) {
                if (err) {
                    console.log(err);
                    callback(null, new Error(500, err));
                } else {
                    console.log(res);
                    callback(response, null);
                }
                dao.disconnect();
            });
        }
    });
}

exports.deleteSongById = function (songId, callback) {
    dao.connect();
    Song.findById(songId, function (error, response) {
        if (error) {
            console.log(error);
            callback(null, new Error(500, error));
            dao.disconnect();
        } else {
            console.log(response);
            Song.remove({ _id: songId }, function (err, res) {
                if (err) {
                    console.log(err);
                    callback(null, new Error(500, err));
                } else {
                    console.log(res);
                    callback(res, null);
                }
                dao.disconnect();
            });
        }
    });
}


exports.increaseSongReproductionsById = function (songId, callback) {
    dao.connect();
    Song.findById(songId).populate( 'albums' ).exec( function (error, response) {
        if (error) {
            console.log(error);
            callback(null, new Error(500, error));
            dao.disconnect();
        } else {
            console.log(response);
            const rep = (response.reproductions) ? response.reproductions + 1 : 1;
            Song.update({ _id: songId }, { reproductions: rep }, function (err, res) {
                if (err) {
                    console.log(err);
                    callback(null, new Error(500, err));
                } else {
                    console.log(res);
                    callback(response, null);
                }
                dao.disconnect();
            });
        }
    });
}