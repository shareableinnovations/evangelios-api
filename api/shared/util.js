var fs = require('fs');

exports.dataUriToBuffer = ( dataUri ) => {
    var regex = /^data:.+\/(.+);base64,(.*)$/;
    var matches = dataUri.match(regex);
    var data = matches[2];
    return new Buffer(data, 'base64');
}

exports.getDataUriFromPath = ( path, contentType ) => {
    var dataUriPrefix = 'data:' + contentType + ';base64,';
    try{
        var imageData = fs.readFileSync( path ).toString('base64');
        return dataUriPrefix + imageData;
    }
    catch(error){
        return '';
    }
    
}

exports.writeFile = ( path, buffer ) => {
    const writeLength = 16000;
    let counter = 0;

    var wstream = fs.createWriteStream( path, { flags: 'w' } );
    wstream.once('open', (fd) => {
        while ( counter < buffer.length ) {
            let chunk;
            if ( writeLength > buffer.length ) {
                chunk = buffer;
            } else {
                chunk = new Buffer(16000);
                buffer.copy( chunk, 0, counter, counter + writeLength );
            }
            wstream.write( chunk );
            counter += writeLength;
        } 
    
        wstream.end();
    });
}