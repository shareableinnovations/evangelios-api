var express = require( 'express' ),
    usersRouter = express.Router(),
    Response = require( '../models/response' ),
    auth = require( '../middlewares/auth.middleware' ),
    Error = require( '../models/error' );

var userController = require( '../controllers/user.controller' );
usersRouter
    // Authenticate user
    .get( '/authenticate', function ( req, res, next ) { 
        console.log( 'GET Request to /users/authenticate' ); 

        req.checkQuery( 'username' ).notEmpty();
        req.checkQuery( 'password' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Data faltantes' ) )
        } else {
            userController.authenticateUser( req.query.username, req.query.password, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })

    // Add an user
    .post( '/', function ( req, res, next ) { 
        console.log( 'POST Request to /users' );

        req.checkBody( 'firstName' ).notEmpty();
        req.checkBody( 'lastName' ).notEmpty();
        req.checkBody( 'email' ).notEmpty();
        req.checkBody( 'role' ).notEmpty();
        req.checkBody( 'password' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            userController.addUser(req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })


    // modify an user
    .put( '/', function ( req, res, next ) { 
        console.log( 'PUT Request to /users' );

        req.checkBody( '_id' ).notEmpty();
        req.checkBody( 'firstName' ).notEmpty();
        req.checkBody( 'lastName' ).notEmpty();
        req.checkBody( 'email' ).notEmpty();
        req.checkBody( 'role' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            userController.updateUser(req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })

    // set favorites
    .put( '/favorites', function ( req, res, next ) { 
        console.log( 'PUT Request to /users/favorites' );
        req.checkBody( '_id' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            userController.updateUserFavorites(req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })

    // modify user password
    .put( '/recover', function ( req, res, next ) { 
        console.log( 'PUT Request to /users' );
        req.checkBody( 'email' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) )
        } else {
            userController.recoverPassword(req.body.email, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })

    // Get users
    .get( '/', function ( req, res, next ) {
        console.log( 'GET Request to /users' ); 

        userController.getUsers(function ( resp, err ) {
            if ( err ) {
                next( err );
            } else {
                var response = new Response( resp, null );
                res.json( response );
            }
        });
    })
    
    //Delete album by id
    .delete( '/:id', function ( req, res, next) {
        console.log( 'DELETE Request to /users/:id' ); 
        
        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();
        
        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) );
        } else {
            userController.deleteUserById( req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    });

module.exports = usersRouter;