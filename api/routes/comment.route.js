var express = require('express'),
    commentRouter = express.Router(),
    Response = require('../models/response'),
    auth = require('../middlewares/auth.middleware'),
    Error = require('../models/error');

var commentController = require('../controllers/comment.controller');
commentRouter

    // Get comments
    .get('/', function (req, res, next) {
        console.log('GET Request to /comments');

        commentController.getComments(function (resp, err) {
            if (err) {
                next(err);
            } else {
                var response = new Response(resp, null);
                res.json(response);
            }
        });
    })


    .post('/', function (req, res, next) {
        console.log('POST Request to /users');

        req.checkBody('user').notEmpty();
        req.checkBody('comment').notEmpty();
        req.checkBody('email').notEmpty();
        req.checkBody('date').notEmpty();

        var errors = req.validationErrors();

        if (errors) {
            next(new Error(400, 'Parametros faltantes'))
        } else {
            commentController.addComment(req.body, function (resp, err) {
                if (err) {
                    next(err);
                } else {
                    var response = new Response(resp, null);
                    res.json(response);
                }
            });
        }
    })
    module.exports = commentRouter;