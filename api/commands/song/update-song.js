var songDao = require( '../../dao/song.dao' );

exports.execute = function ( id, song, callback ) {
    songDao.updateSong( id, song, function ( result, err ) {
        callback( result, err );
    });
}