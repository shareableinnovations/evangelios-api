var express = require( 'express' ),
    categoriesRouter = express.Router(),
    Response = require( '../models/response' ),
    auth = require( '../middlewares/auth.middleware' ),
    Error = require( '../models/error' );

var categoryController = require( '../controllers/category.controller' );
categoriesRouter
    // Add a category
    .post( '/', function ( req, res, next ) { 
        console.log( 'POST Request to /categories' );

        req.checkBody( 'name' ).notEmpty();
        req.checkBody( 'image' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) );
        } else {
            categoryController.addCategory(req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })

    // Get category by id
    .get( '/:id', function ( req, res, next ) {
        console.log( 'GET Request to /categories/:id' ); 

        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Data faltante' ) );
        } else {
            console.log(req.params);
            categoryController.getCategoryById(req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })
    
    // Update a category
    .put( '/:id', function ( req, res, next ) { 
        console.log( 'PUT Request to /categories/:id' );
        
        req.checkParams('id').notEmpty();
        req.checkBody( 'name' ).notEmpty();
        req.checkBody( 'image' ).notEmpty();

        var errors = req.validationErrors();

        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) );
        } else {
            categoryController.updateCategory(req.params.id, req.body, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    })

    // Get categories
    .get( '/', function ( req, res, next ) {
        console.log( 'GET Request to /categories' ); 

        categoryController.getCategories(function ( resp, err ) {
            if ( err ) {
                next( err );
            } else {
                var response = new Response( resp, null );
                res.json( response );
            }
        });
    })
    
    //Delete category by id
    .delete( '/:id', function ( req, res, next) {
        console.log( 'DELETE Request to /categories/:id' ); 
        
        req.checkParams('id').notEmpty();
        var errors = req.validationErrors();
        
        if ( errors ) {
            next( new Error( 400, 'Parametros faltantes' ) );
        } else {
            categoryController.deleteCategoryById( req.params.id, function ( resp, err ) {
                if ( err ) {
                    next( err );
                } else {
                    var response = new Response( resp, null );
                    res.json( response );
                }
            });
        }
    });

module.exports = categoriesRouter;