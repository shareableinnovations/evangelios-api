var albumDao = require( '../../dao/album.dao' );

exports.execute = function ( albumId, callback ) {
    albumDao.deleteAlbumById( albumId , function ( result, err ) {
        callback( result, err );
    });
}