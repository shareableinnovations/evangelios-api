var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
    user: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    comment: String,
    email: String,
    date: Date
});

exports.Comment = mongoose.model('Comment', CommentSchema);